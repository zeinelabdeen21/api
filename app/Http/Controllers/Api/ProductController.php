<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return response()->json([
            'status' => true,
            'data' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:products',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:4000',
            'description' => 'required|string',
            'price_before_discount' => 'required',
            'price' => 'required',
        ]);

        $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
        $imageMove = request()->file('image')->move(public_path('uploads/product_carts/'), $imageName);
        $photoUrl = url('/uploads/product_carts/' . $imageName);
        if (!$photoUrl) {
            return response()->json([
                'status' => false,
                'message' => 'حدث شئ ما خطأ لم يتم رفع الصورة',
            ], 200);
        }
        $create = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $photoUrl,
            'price_before_discount' => $request->price_before_discount,
            'price' => $request->price,
        ]);

        return response()->json([
            'status' => true,
            'message' => 'news succese',
            'data' => $create,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = Product::find($id);

        if(!$new){
            return response()->json([
                'status' => false,
                'message' => 'لا يوجد',
            ],404);
        }

        if($new){
            return response()->json([
                'status' => true,
                'message' => 'ok',
                'data' =>  $new,
            ],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:4000',
            'description' => 'required|string',
            'price_before_discount' => 'required',
            'price' => 'required',
        ]);

        $new = Product::find($id);

        $imageName = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
        $imageMove = request()->file('image')->move(public_path('uploads/product_carts/'), $imageName);
        $photoUrl = url('/uploads/product_carts/' . $imageName);

        $new->update([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $photoUrl,
            'price_before_discount' => $request->price_before_discount,
            'price' => $request->price,
        ]);


        return response()->json([
            'status' => true,
            'message' => 'post succese',
            'data' => $new,
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = Product::find($id);

        if(!$new){
            return response()->json([
                'status' => false,
                'message' => 'delete null',
            ],404);
        }

        $new->delete($id);

        if($new){
            return response()->json([
                'status' => true,
                'message' => 'delete ok',
            ],200);
        }
    }
}
